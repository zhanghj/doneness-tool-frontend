import { delay } from 'roadhog-api-doc';
import data from './src/routes/playback/ResultData';
import { allAnnotationData, allCategory } from './src/utils/data';
import deltaData from './src/routes/summary/DeltaData';
// 是否禁用代理
const noProxy = process.env.NO_PROXY === 'true';

// 代码中会兼容本地 service mock 以及部署站点的静态数据
const proxy = {
  'GET /analysis/getSummaryData' (req, res) {
    let list=[{"NiuJiaoMianBao":"0","SanWenYuPa":"0.39414414414414417","ZhengJi":"0","QuQiBing":"0.55930359085963","JiXiongRou":"0","JiChiGen":"0","GuangShiYueBing":"0","ZhuPaiGu":"0.5711012265804016","YuMi":"0","BingGan":"0.6101124631268436","DanTa":"0.6518246499885242","JiQuanChi":"0","ZhuTi":"0","QieZi":"0","XiangChang":"0.3703053703053703","YouYu":"0","HuaSheng":"0","method":"without_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.0_randhsv0.05_1.1_1.1_nclass14_loss0.20.h5","ZhengYu":"0","PeiGenJuan":"0.6235560588901472","Ya":"0","JiTui":"0","JiZhongChi":"0.6196495037220844","MaoMaoChongMianBao":"0.6145930776426567","TuSi":"0","Pizza":"0","Xia":"0.38203891854658173","ZhuRou":"0","ZhiBeiDanGao":"0","KeSongMianBao":"0","NiuPa":"0","YangPai":"0","QiFengDanGao":"0","HongShuZiShu":"0","TuDou":"0"},{"NiuJiaoMianBao":"0","SanWenYuPa":"0","ZhengJi":"0","QuQiBing":"0.580253723110866","JiXiongRou":"0","JiChiGen":"0","GuangShiYueBing":"0","ZhuPaiGu":"0.549983208328669","YuMi":"0","BingGan":"0.6000556637907042","DanTa":"0.650743220242823","JiQuanChi":"0","ZhuTi":"0","QieZi":"0","XiangChang":"0.36251252923488136","YouYu":"0","HuaSheng":"0","method":"d_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.0_randhsv0.05_1.1_1.1_nclass14_loss0.20.h5","ZhengYu":"0","PeiGenJuan":"0.6403697334479793","Ya":"0","JiTui":"0","JiZhongChi":"0.6177382261773823","MaoMaoChongMianBao":"0","TuSi":"0","Pizza":"0","Xia":"0.3423502354616143","ZhuRou":"0","ZhiBeiDanGao":"0","KeSongMianBao":"0","NiuPa":"0","YangPai":"0","QiFengDanGao":"0","HongShuZiShu":"0","TuDou":"0"}];
    res.send(list);
  },
  'GET /analysis/getDeltaData' (req, res) {
    res.send(deltaData);
  },
  'GET /analysis/getModelList' (req, res) {
    let list=[
      '2step_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.0_randhsv0.05_1.1_1.1_loss0.61.h5_add_time',
      '2step_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.0_randhsv0.05_1.1_1.1_loss0.61.h5_no_time',
      '2step_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.2_loss0.59_curr',
      '2step_doneness_deltanet_concatdelta_densenet121_regression_minusref_r0_grayperc0.2_randhsv0.05_1.1_1.1_loss0.59.h5',
      '2step_doneness_deltanet_densenet121_regression_concat_loss0.37_grayperc0.01',
    ];
    res.send(list);
  },
  'GET /analysis/getCookList' (req, res) {
    let list=[
      { 'cook_time': '2018_08_06_17_30_18', },
      { 'cook_time': '2018_08_06_18_12_26', },
      { 'cook_time': '2018_08_06_18_56_27', },
      { 'cook_time': '2018_08_08_15_22_14', },
      { 'cook_time': '2018_08_08_16_35_14', },
      { 'cook_time': '2018_08_08_17_20_17', },
      { 'cook_time': '2018_08_08_18_14_23', },
      { 'cook_time': '2018_08_08_18_51_28', },
      { 'cook_time': '2018_05_07_08_12_48', },
      { 'cook_time': '2018_05_07_09_07_18', },
      { 'cook_time': '2018_05_07_09_50_18', },
      { 'cook_time': '2018_05_07_09_57_31', },
      { 'cook_time': '2018_05_07_11_26_47', },
      { 'cook_time': '2018_08_10_15_36_49', },
      { 'cook_time': '2018_08_10_16_27_54', },
      { 'cook_time': '2018_08_10_10_27_51', },
      { 'cook_time': '2018_08_10_14_38_14', },
      { 'cook_time': '2018_08_11_14_08_18', },
      { 'cook_time': '2018_08_11_15_26_07', },
      { 'cook_time': '2018_08_11_16_31_29', },
      { 'cook_time': '2018_08_06_15_41_43', },
      { 'cook_time': '2018_08_06_16_49_41', },
    ];
    res.send(list);
  },
  'GET /analysis/getTimeInterval' (req, res) {
      const list = [
        {
            "experiment": "17:36:25-17:54:40",
            "human": "17:36:25-17:44:50"
        },
        {
            "experiment": "17:54:55-18:00:40",
            "human": "17:44:51-17:50:00"
        },
        {
            "experiment": "18:00:55-18:01:25",
            "human": "17:50:01-17:59:00"
        },
        {
            "experiment": "18:01:40-18:03:55",
            "human": "17:59:01-18:04:00"
        },
        {
            "experiment": null,
            "human": "3"
        }
    ];
    res.send(list);
  },
  'GET /analysis/getCookData' (req, res) {
    res.send(data);
  },
  'GET /analysis/getNewestPic' (req, res) {
    var url = 'http://47.106.93.196/uploads/2018/09/21/SDM0501N74900008/2018_09_21_15_16_32.jpg';
    res.send({
      url,
    });
  },
  'GET /analysis/getNewestMaskPic' (req, res) {
    var url = 'http://47.106.93.196/mask/23_92/res_fig.jpg';
    res.send({
      url,
    });
  },
  'GET /analysis/getShowSN' (req, res) {
    res.send([
      "SDM0501N74900117",
      // "HLDM0501N82100003",
    ]);
  },

  'GET /analysis/annotation/allCategory' (req, res) {
    res.send(allCategory);
  },
  'GET /analysis/annotation/allFiles' (req, res) {
    res.send(allAnnotationData);
  },
  'GET /analysis/annotation/detail/:filename' (req, res) {
    console.log(req.params);
    res.send([
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1481.jpg",
          "doneness": [
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1482.jpg",
          "doneness": [
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1,
              1
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1483.jpg",
          "doneness": [
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2,
              2
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1484.jpg",
          "doneness": [
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3,
              3
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1485.jpg",
          "doneness": [
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4,
              4
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1486.jpg",
          "doneness": [
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5,
              5
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1487.jpg",
          "doneness": [
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6,
              6
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1488.jpg",
          "doneness": [
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1489.jpg",
          "doneness": [
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8,
              8
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1490.jpg",
          "doneness": [
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9,
              9
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1491.jpg",
          "doneness": [
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10,
              10
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1492.jpg",
          "doneness": [
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11,
              11
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1493.jpg",
          "doneness": [
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12,
              12
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1494.jpg",
          "doneness": [
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13,
              13
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1495.jpg",
          "doneness": [
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14,
              14
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1496.jpg",
          "doneness": [
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15,
              15
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1497.jpg",
          "doneness": [
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16,
              16
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1498.jpg",
          "doneness": [
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17,
              17
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1499.jpg",
          "doneness": [
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18,
              18
          ]
      },
      {
          "image": "http://47.106.93.196/rawfood_coco_all/train2018//1500.jpg",
          "doneness": [
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19,
              19
          ]
      }
    ]);
  },

  'GET /analysis/evaluation/categories' (req, res) {
    res.send([
      {
        "id": 1,
        "name": "TuSi"
      },
      {
        "id": 2,
        "name": "XiangChang"
      },
      {
        "id": 3,
        "name": "ZhuPaiGu"
      },
      {
        "id": 4,
        "name": "ZhuRou"
      },
      {
        "id": 5,
        "name": "YouYu"
      },
      {
        "id": 6,
        "name": "ZhengYu"
      },
      {
        "id": 7,
        "name": "YuMi"
      },
      {
        "id": 8,
        "name": "HongShuZiShu"
      },
      {
        "id": 9,
        "name": "HuaSheng"
      },
      {
        "id": 10,
        "name": "JiChiGen"
      },
      {
        "id": 11,
        "name": "JiQuanChi"
      },
      {
        "id": 12,
        "name": "JiZhongChi"
      },
      {
        "id": 13,
        "name": "JiXiongRou"
      },
      {
        "id": 14,
        "name": "JiTui"
      },
      {
        "id": 15,
        "name": "ZhengJi"
      },
      {
        "id": 16,
        "name": "PeiGenJuan"
      },
      {
        "id": 17,
        "name": "TuDou"
      },
      {
        "id": 18,
        "name": "MaoMaoChongMianBao"
      },
      {
        "id": 19,
        "name": "NiuJiaoMianBao"
      },
      {
        "id": 20,
        "name": "ZhiBeiDanGao"
      },
      {
        "id": 21,
        "name": "DanTa"
      },
      {
        "id": 22,
        "name": "QuQiBing"
      },
      {
        "id": 23,
        "name": "BingGan"
      },
      {
        "id": 24,
        "name": "Pizza"
      },
      {
        "id": 25,
        "name": "NiuPa"
      },
      {
        "id": 26,
        "name": "YangPai"
      },
      {
        "id": 27,
        "name": "SanWenYuPa"
      },
      {
        "id": 28,
        "name": "Xia"
      },
      {
        "id": 29,
        "name": "QiFengDanGao"
      },
      {
        "id": 30,
        "name": "GuangShiYueBing"
      },
      {
        "id": 31,
        "name": "ZhuTi"
      },
      {
        "id": 32,
        "name": "Ya"
      },
      {
        "id": 33,
        "name": "QieZi"
      },
      {
        "id": 34,
        "name": "KeSongMianBao"
      },
      {
        "id": 100,
        "name": "KongZai"
      }
    ]);
  },
  'GET /analysis/evaluation/dirs' (req, res) {
    res.send([
      {
        "value": "logs_doneness_devided_L1_kz_13",
        "label": "logs_doneness_devided_L1_kz_13",
        "children": [
          {
            "value": "20181019T1732",
            "label": "20181019T1732"
          }
        ]
      },
      {
        "value": "logs_doneness_devided_L1_kz_13_imgsz512_",
        "label": "logs_doneness_devided_L1_kz_13_imgsz512_",
        "children": [
          {
            "value": "20181019T1732",
            "label": "20181019T1732"
          }
        ]
      }
    ]);
  },
  'GET /analysis/evaluation/result' (req, res) {
    res.send([
      {
        "filename": "10_instances_val2018.json",
        "imagesSize": 20,
        "detailList": [
          {
            "imageId": 1801,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1801.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1802,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1802.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1803,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1803.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1804,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1804.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1805,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1805.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1806,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1806.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1807,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1807.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1808,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1808.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1809,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1809.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1810,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1810.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1811,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1811.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1812,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1812.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1813,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1813.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1814,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1814.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1815,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1815.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1816,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1816.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1817,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1817.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1818,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1818.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1819,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1819.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1820,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1820.png",
            "annotationsSize": 12
          }
        ]
      },
      {
        "filename": "12_instances_val2018.json",
        "imagesSize": 20,
        "detailList": [
          {
            "imageId": 1201,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1201.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1202,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1202.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1203,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1203.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1204,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1204.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1205,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1205.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1206,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1206.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1207,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1207.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1208,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1208.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1209,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1209.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1210,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1210.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1211,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1211.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1212,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1212.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1213,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1213.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1214,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1214.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1215,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1215.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1216,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1216.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1217,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1217.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1218,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1218.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1219,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1219.png",
            "annotationsSize": 24
          },
          {
            "imageId": 1220,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1220.png",
            "annotationsSize": 24
          }
        ]
      },
      {
        "filename": "14_instances_val2018.json",
        "imagesSize": 20,
        "detailList": [
          {
            "imageId": 1621,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1621.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1622,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1622.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1623,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1623.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1624,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1624.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1625,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1625.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1626,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1626.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1627,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1627.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1628,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1628.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1629,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1629.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1630,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1630.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1631,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1631.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1632,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1632.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1633,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1633.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1634,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1634.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1635,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1635.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1636,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1636.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1637,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1637.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1638,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1638.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1639,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1639.png",
            "annotationsSize": 12
          },
          {
            "imageId": 1640,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1640.png",
            "annotationsSize": 12
          }
        ]
      },
      {
        "filename": "1_instances_val2018.json",
        "imagesSize": 20,
        "detailList": [
          {
            "imageId": 1541,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1541.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1542,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1542.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1543,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1543.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1544,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1544.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1545,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1545.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1546,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1546.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1547,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1547.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1548,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1548.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1549,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1549.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1550,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1550.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1551,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1551.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1552,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1552.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1553,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1553.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1554,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1554.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1555,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1555.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1556,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1556.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1557,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1557.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1558,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1558.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1559,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1559.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1560,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1560.png",
            "annotationsSize": 9
          }
        ]
      },
      {
        "filename": "2_instances_val2018.json",
        "imagesSize": 20,
        "detailList": [
          {
            "imageId": 1401,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1401.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1402,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1402.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1403,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1403.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1404,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1404.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1405,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1405.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1406,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1406.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1407,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1407.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1408,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1408.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1409,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1409.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1410,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1410.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1411,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1411.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1412,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1412.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1413,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1413.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1414,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1414.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1415,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1415.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1416,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1416.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1417,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1417.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1418,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1418.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1419,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1419.png",
            "annotationsSize": 9
          },
          {
            "imageId": 1420,
            "imageUrl": "http://47.106.93.196/gap_analysis//logs_doneness_devided_L1_kz_13/20181019T1732/segmentions_result/res_fig_1420.png",
            "annotationsSize": 9
          }
        ]
      }
    ])
  },
  'GET /analysis/evaluation/getTrainStats' (req, res) {
    res.send([
      {
        "categoryId": 2,
        "categoryName": "XiangChang",
        "imagesSize": 800,
        "instancesSize": 8740
      },
      {
        "categoryId": 3,
        "categoryName": "ZhuPaiGu",
        "imagesSize": 780,
        "instancesSize": 6000
      },
      {
        "categoryId": 100,
        "categoryName": "KongZai",
        "imagesSize": 99,
        "instancesSize": 99
      },
      {
        "categoryId": 9,
        "categoryName": "HuaSheng",
        "imagesSize": 240,
        "instancesSize": 240
      },
      {
        "categoryId": 12,
        "categoryName": "JiZhongChi",
        "imagesSize": 920,
        "instancesSize": 9098
      },
      {
        "categoryId": 16,
        "categoryName": "PeiGenJuan",
        "imagesSize": 600,
        "instancesSize": 7360
      },
      {
        "categoryId": 18,
        "categoryName": "MaoMaoChongMianBao",
        "imagesSize": 220,
        "instancesSize": 2040
      },
      {
        "categoryId": 21,
        "categoryName": "DanTa",
        "imagesSize": 760,
        "instancesSize": 8130
      },
      {
        "categoryId": 22,
        "categoryName": "QuQiBing",
        "imagesSize": 600,
        "instancesSize": 12100
      },
      {
        "categoryId": 23,
        "categoryName": "BingGan",
        "imagesSize": 760,
        "instancesSize": 15890
      },
      {
        "categoryId": 24,
        "categoryName": "Pizza",
        "imagesSize": 540,
        "instancesSize": 660
      },
      {
        "categoryId": 27,
        "categoryName": "SanWenYuPa",
        "imagesSize": 560,
        "instancesSize": 1620
      },
      {
        "categoryId": 28,
        "categoryName": "Xia",
        "imagesSize": 880,
        "instancesSize": 11948
      },
      {
        "categoryId": 30,
        "categoryName": "GuangShiYueBing",
        "imagesSize": 560,
        "instancesSize": 7960
      }
    ]);
  },
  'GET /analysis/evaluation/getValStats' (req, res) {
    res.send([
      {
        "categoryId": 21,
        "categoryName": "DanTa",
        "imagesSize": 80,
        "instancesSize": 800
      },
      {
        "categoryId": 23,
        "categoryName": "BingGan",
        "imagesSize": 100,
        "instancesSize": 1320
      },
      {
        "categoryId": 28,
        "categoryName": "Xia",
        "imagesSize": 80,
        "instancesSize": 1320
      },
      {
        "categoryId": 12,
        "categoryName": "JiZhongChi",
        "imagesSize": 80,
        "instancesSize": 800
      }
    ]);
  },
  'GET /analysis/evaluation/getTestStats' (req, res) {
    res.send([
      {
        "categoryId": 21,
        "categoryName": "DanTa",
        "imagesSize": 80,
        "instancesSize": 800
      },
      {
        "categoryId": 23,
        "categoryName": "BingGan",
        "imagesSize": 100,
        "instancesSize": 1320
      },
      {
        "categoryId": 28,
        "categoryName": "Xia",
        "imagesSize": 80,
        "instancesSize": 1320
      },
      {
        "categoryId": 12,
        "categoryName": "JiZhongChi",
        "imagesSize": 80,
        "instancesSize": 800
      }
    ]);
  },
  'GET /analysis/evaluation/getConfusionStats' (req, res) {
    res.send([
      {
        "categoryName": "DanTa",
        "matchCount": 958,
        "totalCount": 997,
        "matchPercent": 0.9608826479438315,
        "rawData": "958 0 0 0 0 0 0 0 0 0 0 0 0 39 "
      },
      {
        "categoryName": "BingGan",
        "matchCount": 1610,
        "totalCount": 1648,
        "matchPercent": 0.9769417475728155,
        "rawData": "1 1610 0 0 0 0 0 0 0 0 0 0 0 37 "
      },
      {
        "categoryName": "Xia",
        "matchCount": 1215,
        "totalCount": 1406,
        "matchPercent": 0.864153627311522,
        "rawData": "0 0 1215 5 1 82 0 0 4 0 0 0 0 99 "
      },
      {
        "categoryName": "JiZhongChi",
        "matchCount": 623,
        "totalCount": 886,
        "matchPercent": 0.7031602708803611,
        "rawData": "0 9 18 623 0 0 0 0 1 0 1 0 113 121 "
      },
      {
        "categoryName": "QuQiBing",
        "matchCount": 1133,
        "totalCount": 1380,
        "matchPercent": 0.8210144927536231,
        "rawData": "0 169 0 0 1133 0 0 0 0 0 0 0 0 78 "
      },
      {
        "categoryName": "PeiGenJuan",
        "matchCount": 1086,
        "totalCount": 1120,
        "matchPercent": 0.9696428571428571,
        "rawData": "0 0 2 1 0 1086 0 0 0 0 0 0 1 30 "
      },
      {
        "categoryName": "HuaSheng",
        "matchCount": 80,
        "totalCount": 80,
        "matchPercent": 1,
        "rawData": "0 0 0 0 0 0 80 0 0 0 0 0 0 0 "
      },
      {
        "categoryName": "Pizza",
        "matchCount": 75,
        "totalCount": 80,
        "matchPercent": 0.9375,
        "rawData": "0 0 0 0 0 0 0 75 0 0 0 0 4 1 "
      },
      {
        "categoryName": "SanWenYuPa",
        "matchCount": 27,
        "totalCount": 467,
        "matchPercent": 0.057815845824411134,
        "rawData": "0 0 9 24 1 0 0 7 27 0 9 18 60 312 "
      },
      {
        "categoryName": "MaoMaoChongMianBao",
        "matchCount": 76,
        "totalCount": 700,
        "matchPercent": 0.10857142857142857,
        "rawData": "0 0 2 98 7 0 0 0 36 76 88 78 0 315 "
      },
      {
        "categoryName": "GuangShiYueBing",
        "matchCount": 651,
        "totalCount": 1000,
        "matchPercent": 0.651,
        "rawData": "0 3 0 0 191 0 0 0 0 0 651 0 0 155 "
      },
      {
        "categoryName": "XiangChang",
        "matchCount": 546,
        "totalCount": 680,
        "matchPercent": 0.8029411764705883,
        "rawData": "0 0 0 0 0 0 0 0 2 0 0 546 56 76 "
      },
      {
        "categoryName": "ZhuPaiGu",
        "matchCount": 297,
        "totalCount": 380,
        "matchPercent": 0.781578947368421,
        "rawData": "0 0 4 1 0 12 0 0 1 0 10 6 297 49 "
      }
    ]);
  },

};

export default (noProxy ? {} : delay(proxy, 1000));
