import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './CommonImage.less';

export default class Image extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
  }

  render() {
    return (
      <div className={styles.wrap}>
        <img className={styles['poll-image']} alt={''} src={this.props.src} />
      </div>
    )
  }
}
