import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './CommonImage.less';
import { getNewestMaskPic } from '../services/analysis';

export default class MaskPollImage extends Component {
  static propTypes = {
    // prop: PropTypes
    sn: PropTypes.string.isRequired,
  }

  state = {
    pollImageUrl: '',
  }

  doGetNewestMaskPic = () => {
    const { sn } = this.props;
    getNewestMaskPic(sn).then(data => {
      this.setState({
        pollImageUrl: data.url,
      });
    })
  }

  componentDidMount() {
    this.timeId = setInterval(() => {
      this.doGetNewestMaskPic();
    },10000);
    this.doGetNewestMaskPic();
  }

  componentWillUnmount() {
    if(this.timeId) {
      clearInterval(this.timeId);
      this.timeId = null;
    }
  }

  render() {
    const { pollImageUrl } = this.state;

    if(!pollImageUrl) {
      return <div></div>;
    }

    console.log(pollImageUrl);

    return (
      <div className={styles.wrap}>
        <img className={styles['poll-image']} alt={''} src={pollImageUrl} />
      </div>
    )
  }
}
