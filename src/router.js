import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import IndexPage from './routes/IndexPage';
import PlaybackDetailPage from './routes/playback/PlaybackDetailPage';
import PlaybackSelectPage from './routes/playback/PlaybackSelectPage';
import ShowPage from './routes/ShowPage';
import SummaryPage from './routes/summary/SummaryPage';
import AnnotationListPage from './routes/annotation/AnnotationListPage';
import AnnotationDetailPage from './routes/annotation/AnnotationDetailPage';
import EvaluationListPage from './routes/evaluation/EvaluationListPage';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={IndexPage} />
        <Route path="/summary" exact component={SummaryPage} />
        <Route path="/select/:method" component={PlaybackSelectPage} />
        <Route path="/select" component={PlaybackSelectPage} />
        <Route path="/annotation/list" component={AnnotationListPage} />
        <Route path="/annotation/detail/:filename" component={AnnotationDetailPage} />
        <Route path="/result/:model/:time" exact component={PlaybackDetailPage} />
        <Route path="/show" exact component={ShowPage} />
        <Route path="/evaluation/list" component={EvaluationListPage} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
