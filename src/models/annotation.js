import update from 'immutability-helper';
import { getAllCategory, getAllAnnotationFiles } from '../services/annotation';
import { allCategory } from '../utils/data';

export default {

  namespace: 'annotation',

  state: {
    selectedCategory: '',
    category: allCategory,
    allAnnotationFiles: null,
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    // *fetchAllCategory(action, { call, put }) {
    //   const response = yield call(getAllCategory);
    //   yield put({
    //     type: 'save',
    //     payload: {
    //       key: 'category',
    //       value: response,
    //     },
    //   });
    // },
    *fetchAllAnnotationFiles({ payload }, { call, put }) {
      const response = yield call(getAllAnnotationFiles);
      yield put({
        type: 'save',
        payload: {
          key: 'allAnnotationFiles',
          value: response,
        },
      });
    },
  },

  reducers: {
    save(state, { payload : { key, value }}) {
      return update(state, { [key] : { $set: value}});
    },
    handleCategoryChange(state, { payload : { selectedCategory }}) {
      return update(state, { selectedCategory: { $set: selectedCategory }});
    },
  },

};
