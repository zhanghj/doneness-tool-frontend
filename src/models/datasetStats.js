import update from 'immutability-helper';
import { getTrainStats, getValStats, getTestStats } from '../services/datasetStats';

export default {

  namespace: 'datasetStats',

  state: {
    train: [],
    val: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *fetchDatasetStats({ payload : { type } }, { call, put }) {
      let response;
      console.log(type);
      if(type === 'train'){
        response = yield call(getTrainStats);
      }else if (type === 'val'){
        response = yield call(getValStats);
      }else if (type === 'test'){
        response = yield call(getTestStats);
      }else {
        throw new Error("payload type must be 'train' or 'val' or 'test'");
      }
      yield put({
        type: 'save',
        payload: {
          key: type,
          value: response,
        },
      });
    },
  },

  reducers: {
    save(state, { payload : { key, value }}) {
      return update(state, { [key] : { $set: value}});
    },
  },

};
