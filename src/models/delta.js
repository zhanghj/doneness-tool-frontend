import update from 'immutability-helper';
import { getEvaluationDirs } from '../services/evaluation';
import { getDeltaData } from '../services/analysis';

export default {

  namespace: 'delta',

  state: {
    data: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *fetchDeltaData(action, { call, put }) {
      const response = yield call(getDeltaData);
      yield put({
        type: 'save',
        payload: {
          key: 'data',
          value: response,
        },
      });
    },
  },

  reducers: {
    save(state, { payload : { key, value }}) {
      return update(state, { [key] : { $set: value}});
    },
  },

};
