import update from 'immutability-helper';
import { getConfusionStats } from '../services/confusionStats';

export default {

  namespace: 'confusionStats',

  state: {
    stats: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *fetchConfustionStats({ payload : { dir } }, { call, put }) {
      const response = yield call(getConfusionStats, dir);

      yield put({
        type: 'save',
        payload: {
          key: 'stats',
          value: response,
        },
      });
    },
  },

  reducers: {
    save(state, { payload : { key, value }}) {
      return update(state, { [key] : { $set: value}});
    },
  },

};
