import request from '../utils/request';

export function getSummaryData() {
  return request('/analysis/getSummaryData');
}

export function getDeltaData() {
  return request('/analysis/getDeltaData');
}

export function getNewestPic(sn) {
  return request(`/analysis/getNewestPic?sn=${sn}`);
}

export function getNewestMaskPic(sn) {
  return request(`/analysis/getNewestMaskPic?sn=${sn}`);
}

export function getShowSN() {
  return request(`/analysis/getShowSN`);
}

