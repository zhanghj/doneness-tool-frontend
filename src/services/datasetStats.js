import request from '../utils/request';

export function getTrainStats() {
  return request('/analysis/evaluation/getTrainStats');
}

export function getValStats() {
  return request(`/analysis/evaluation/getValStats`);
}

export function getTestStats() {
  return request(`/analysis/evaluation/getTestStats`);
}
