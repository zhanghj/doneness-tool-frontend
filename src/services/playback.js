import request from '../utils/request';

export function getModelList() {
  return request('/analysis/getModelList');
}

export function getCookList(model) {
  return request(`/analysis/getCookList?model=${model}`);
}

export function getCookData(model, cook) {
  return request(`/analysis/getCookData?model=${model}&cook=${cook}`);
}

export function getTimeInterval(model, cook) {
  return request(`/analysis/getTimeInterval?model=${model}&cook=${cook}`);
}
