import request from '../utils/request';

export function getConfusionStats(dir) {
  return request(`/analysis/evaluation/getConfusionStats?dir=${dir}`);
}
