import request from '../utils/request';

export function getEvaluationDirs() {
  return request('/analysis/evaluation/dirs');
}

export function getCategories() {
  return request(`/analysis/evaluation/categories`);
}

export function getEvaluationResult(dir, categoryId) {
  return request(`/analysis/evaluation/result?dir=${dir}&categoryId=${categoryId}`);
}
