import request from '../utils/request';

export function getAllCategory() {
  return request('/analysis/annotation/allCategory');
}

export function getAllAnnotationFiles() {
  return request(`/analysis/annotation/allFiles`);
}

// export function getAnnotationDetail(filename) {
//   return request(`/analysis/annotation/detail/${filename}`);
// }
