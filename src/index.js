import dva from 'dva';
import './index.css';

// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/example').default);
app.model(require('./models/annotation').default);
app.model(require('./models/evaluation').default);
app.model(require('./models/datasetStats').default);
app.model(require('./models/confusionStats').default);
app.model(require('./models/playback').default);
app.model(require('./models/delta').default);


// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');
