import { allCategory } from './data';

export function getCategoryName(categoryId){
  const categories = allCategory.filter(i => {
    return i.id == categoryId;
  });

  if(categories.length > 0){
    return categories[0].name;
  }else{
    return "unknown";
  };
}

export function getCategoryNames(categorySet){
  return categorySet.map(categoryId => getCategoryName(categoryId)).join(",");
}

export function checkNumber(theObj) {
  var reg = /^[0-9]+.?[0-9]*$/;
  if (reg.test(theObj)) {
    return true;
  }
  return false;
}
