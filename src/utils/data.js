export const allAnnotationData = [
  {
      "filename": "10001_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 9261,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9261.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9262,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9262.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9263,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9263.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9264,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9264.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9265,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9265.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9266,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9266.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9267,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9267.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9268,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9268.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9269,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9269.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9270,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9270.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9271,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9271.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9272,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9272.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9273,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9273.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9274,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9274.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9275,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9275.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9276,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9276.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9277,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9277.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9278,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9278.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9279,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9279.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9280,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9280.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          }
      ]
  },
  {
      "filename": "10002_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 9281,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9281.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9282,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9282.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9283,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9283.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9284,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9284.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9285,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9285.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9286,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9286.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9287,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9287.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9288,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9288.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9289,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9289.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9290,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9290.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9291,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9291.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9292,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9292.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9293,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9293.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9294,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9294.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9295,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9295.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9296,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9296.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9297,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9297.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9298,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9298.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9299,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9299.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9300,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9300.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          }
      ]
  },
  {
      "filename": "10003_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 9301,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9301.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9302,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9302.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9303,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9303.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9304,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9304.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9305,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9305.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9306,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9306.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9307,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9307.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9308,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9308.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9309,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9309.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9310,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9310.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9311,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9311.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9312,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9312.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9313,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9313.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9314,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9314.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9315,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9315.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9316,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9316.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9317,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9317.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9318,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9318.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9319,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9319.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9320,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9320.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          }
      ]
  },
  {
      "filename": "10004_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 9321,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9321.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9322,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9322.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9323,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9323.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9324,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9324.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9325,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9325.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9326,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9326.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9327,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9327.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9328,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9328.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9329,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9329.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9330,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9330.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9331,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9331.jpg",
              "doneness": [
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9332,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9332.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9333,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9333.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9334,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9334.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9335,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9335.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9336,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9336.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9337,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9337.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9338,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9338.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9339,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9339.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 9340,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/9340.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0
              ]
          }
      ]
  },
  {
      "filename": "103_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1021,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1021.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1022,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1022.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1023,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1023.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1024,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1024.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1025,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1025.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1026,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1026.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1027,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1027.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1028,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1028.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1029,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1029.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1030,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1030.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1031,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1031.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1032,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1032.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1033,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1033.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1034,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1034.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1035,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1035.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1036,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1036.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1037,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1037.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1038,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1038.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1039,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1039.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1040,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1040.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "107_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 961,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/961.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 962,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/962.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 963,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/963.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 964,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/964.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 965,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/965.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 966,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/966.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 967,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/967.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 968,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/968.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 969,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/969.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 970,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/970.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 971,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/971.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 972,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/972.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 973,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/973.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 974,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/974.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 975,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/975.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 976,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/976.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 977,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/977.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 978,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/978.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 979,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/979.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 980,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/980.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "109_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 681,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/681.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 682,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/682.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 683,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/683.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 684,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/684.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 685,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/685.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 686,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/686.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 687,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/687.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 688,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/688.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 689,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/689.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 690,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/690.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 691,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/691.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 692,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/692.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 693,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/693.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 694,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/694.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 695,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/695.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 696,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/696.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 697,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/697.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 698,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/698.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 699,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/699.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 700,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/700.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "110_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 621,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/621.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 622,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/622.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 623,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/623.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 624,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/624.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 625,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/625.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 626,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/626.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 627,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/627.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 628,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/628.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 629,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/629.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 630,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/630.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 631,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/631.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 632,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/632.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 633,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/633.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 634,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/634.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 635,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/635.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 636,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/636.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 637,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/637.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 638,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/638.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 639,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/639.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 640,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/640.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "114_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1081,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1081.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1082,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1082.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1083,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1083.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1084,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1084.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1085,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1085.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1086,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1086.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1087,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1087.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1088,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1088.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1089,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1089.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1090,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1090.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1091,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1091.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1092,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1092.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1093,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1093.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1094,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1094.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1095,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1095.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1096,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1096.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1097,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1097.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1098,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1098.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1099,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1099.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1100,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1100.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "16_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 601,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/601.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 602,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/602.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 603,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/603.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 604,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/604.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 605,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/605.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 606,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/606.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 607,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/607.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 608,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/608.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 609,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/609.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 610,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/610.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 611,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/611.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 612,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/612.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 613,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/613.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 614,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/614.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 615,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/615.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 616,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/616.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 617,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/617.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 618,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/618.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 619,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/619.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 620,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/620.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "18_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 581,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/581.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 582,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/582.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 583,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/583.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 584,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/584.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 585,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/585.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 586,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/586.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 587,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/587.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 588,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/588.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 589,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/589.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 590,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/590.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 591,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/591.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 592,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/592.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 593,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/593.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 594,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/594.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 595,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/595.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 596,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/596.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 597,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/597.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 598,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/598.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 599,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/599.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 600,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/600.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "20_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2181,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2181.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2182,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2182.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2183,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2183.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2184,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2184.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2185,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2185.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2186,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2186.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2187,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2187.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2188,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2188.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2189,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2189.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2190,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2190.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2191,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2191.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2192,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2192.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2193,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2193.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2194,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2194.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2195,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2195.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2196,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2196.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2197,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2197.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2198,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2198.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2199,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2199.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2200,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2200.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "21_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 721,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/721.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 722,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/722.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 723,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/723.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 724,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/724.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 725,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/725.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 726,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/726.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 727,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/727.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 728,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/728.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 729,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/729.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 730,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/730.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 731,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/731.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 732,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/732.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 733,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/733.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 734,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/734.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 735,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/735.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 736,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/736.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 737,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/737.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 738,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/738.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 739,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/739.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 740,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/740.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "23_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1061,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1061.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1062,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1062.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1063,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1063.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1064,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1064.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1065,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1065.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1066,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1066.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1067,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1067.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1068,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1068.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1069,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1069.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1070,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1070.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1071,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1071.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1072,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1072.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1073,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1073.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1074,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1074.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1075,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1075.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1076,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1076.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1077,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1077.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1078,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1078.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1079,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1079.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1080,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1080.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "24_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2161,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2161.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2162,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2162.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2163,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2163.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2164,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2164.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2165,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2165.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2166,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2166.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2167,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2167.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2168,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2168.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2169,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2169.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2170,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2170.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2171,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2171.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2172,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2172.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2173,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2173.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2174,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2174.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2175,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2175.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2176,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2176.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2177,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2177.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2178,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2178.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2179,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2179.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2180,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2180.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "25_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 821,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/821.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 822,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/822.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 823,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/823.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 824,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/824.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 825,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/825.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 826,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/826.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 827,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/827.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 828,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/828.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 829,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/829.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 830,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/830.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 831,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/831.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 832,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/832.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 833,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/833.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 834,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/834.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 835,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/835.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 836,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/836.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 837,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/837.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 838,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/838.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 839,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/839.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 840,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/840.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "26_instances_val2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1001,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1001.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1002,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1002.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1003,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1003.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1004,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1004.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1005,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1005.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1006,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1006.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1007,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1007.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1008,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1008.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1009,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1009.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1010,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1010.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1011,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1011.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1012,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1012.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1013,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1013.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1014,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1014.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1015,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1015.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1016,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1016.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1017,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1017.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1018,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1018.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1019,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1019.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1020,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/1020.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "27_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1821,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1821.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1822,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1822.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1823,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1823.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1824,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1824.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1825,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1825.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1826,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1826.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1827,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1827.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1828,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1828.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1829,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1829.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1830,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1830.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1831,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1831.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1832,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1832.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1833,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1833.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1834,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1834.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1835,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1835.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1836,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1836.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1837,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1837.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1838,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1838.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1839,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1839.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1840,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1840.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "28_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1881,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1881.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1882,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1882.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1883,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1883.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1884,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1884.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1885,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1885.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1886,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1886.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1887,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1887.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1888,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1888.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1889,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1889.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1890,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1890.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1891,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1891.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1892,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1892.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1893,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1893.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1894,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1894.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1895,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1895.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1896,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1896.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1897,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1897.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1898,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1898.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1899,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1899.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1900,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1900.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "29_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1121,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1121.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1122,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1122.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1123,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1123.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1124,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1124.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1125,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1125.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1126,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1126.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1127,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1127.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1128,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1128.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1129,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1129.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1130,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1130.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1131,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1131.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1132,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1132.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1133,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1133.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1134,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1134.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1135,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1135.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1136,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1136.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1137,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1137.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1138,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1138.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1139,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1139.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1140,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1140.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "30_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1841,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1841.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1842,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1842.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1843,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1843.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1844,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1844.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1845,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1845.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1846,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1846.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1847,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1847.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1848,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1848.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1849,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1849.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1850,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1850.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1851,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1851.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1852,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1852.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1853,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1853.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1854,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1854.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1855,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1855.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1856,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1856.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1857,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1857.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1858,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1858.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1859,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1859.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1860,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1860.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "32_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 781,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/781.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 782,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/782.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 783,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/783.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 784,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/784.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 785,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/785.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 786,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/786.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 787,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/787.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 788,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/788.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 789,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/789.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 790,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/790.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 791,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/791.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 792,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/792.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 793,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/793.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 794,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/794.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 795,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/795.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 796,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/796.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 797,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/797.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 798,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/798.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 799,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/799.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 800,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/800.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "35_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1861,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1861.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1862,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1862.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1863,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1863.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1864,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1864.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1865,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1865.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1866,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1866.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1867,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1867.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1868,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1868.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1869,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1869.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1870,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1870.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1871,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1871.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1872,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1872.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1873,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1873.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1874,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1874.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1875,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1875.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1876,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1876.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1877,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1877.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1878,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1878.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1879,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1879.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1880,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1880.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "36_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 761,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/761.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 762,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/762.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 763,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/763.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 764,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/764.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 765,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/765.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 766,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/766.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 767,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/767.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 768,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/768.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 769,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/769.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 770,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/770.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 771,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/771.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 772,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/772.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 773,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/773.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 774,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/774.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 775,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/775.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 776,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/776.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 777,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/777.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 778,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/778.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 779,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/779.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 780,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/780.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "37_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 941,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/941.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 942,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/942.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 943,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/943.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 944,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/944.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 945,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/945.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 946,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/946.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 947,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/947.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 948,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/948.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 949,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/949.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 950,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/950.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 951,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/951.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 952,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/952.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 953,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/953.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 954,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/954.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 955,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/955.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 956,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/956.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 957,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/957.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 958,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/958.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 959,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/959.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 960,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/960.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "43_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 701,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/701.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 702,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/702.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 703,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/703.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 704,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/704.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 705,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/705.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 706,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/706.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 707,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/707.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 708,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/708.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 709,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/709.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 710,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/710.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 711,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/711.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 712,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/712.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 713,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/713.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 714,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/714.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 715,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/715.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 716,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/716.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 717,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/717.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 718,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/718.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 719,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/719.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 720,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/720.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "44_instances_val2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2361,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2361.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2362,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2362.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2363,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2363.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2364,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2364.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2365,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2365.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2366,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2366.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2367,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2367.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2368,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2368.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2369,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2369.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2370,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2370.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2371,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2371.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2372,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2372.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2373,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2373.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2374,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2374.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2375,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2375.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2376,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2376.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2377,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2377.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2378,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2378.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2379,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2379.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2380,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/2380.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "46_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 561,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/561.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 562,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/562.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 563,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/563.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 564,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/564.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 565,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/565.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 566,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/566.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 567,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/567.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 568,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/568.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 569,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/569.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 570,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/570.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 571,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/571.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 572,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/572.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 573,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/573.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 574,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/574.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 575,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/575.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 576,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/576.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 577,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/577.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 578,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/578.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 579,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/579.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 580,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/580.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "47_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 841,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/841.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 842,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/842.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 843,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/843.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 844,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/844.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 845,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/845.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 846,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/846.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 847,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/847.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 848,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/848.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 849,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/849.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 850,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/850.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 851,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/851.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 852,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/852.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 853,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/853.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 854,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/854.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 855,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/855.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 856,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/856.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 857,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/857.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 858,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/858.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 859,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/859.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 860,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/860.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "51_instances_val2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 861,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/861.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 862,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/862.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 863,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/863.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 864,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/864.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 865,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/865.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 866,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/866.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 867,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/867.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 868,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/868.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 869,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/869.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 870,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/870.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 871,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/871.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 872,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/872.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 873,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/873.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 874,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/874.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 875,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/875.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 876,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/876.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 877,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/877.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 878,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/878.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 879,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/879.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 880,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/880.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "52_instances_val2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 881,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/881.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 882,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/882.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 883,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/883.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 884,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/884.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 885,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/885.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 886,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/886.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 887,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/887.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 888,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/888.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 889,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/889.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 890,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/890.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 891,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/891.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 892,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/892.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 893,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/893.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 894,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/894.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 895,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/895.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 896,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/896.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 897,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/897.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 898,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/898.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 899,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/899.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 900,
              "image": "http://47.106.93.196/rawfood_coco_all/val2018/900.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "55_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1041,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1041.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1042,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1042.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1043,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1043.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1044,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1044.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1045,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1045.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1046,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1046.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1047,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1047.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1048,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1048.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1049,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1049.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1050,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1050.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1051,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1051.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1052,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1052.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1053,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1053.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1054,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1054.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1055,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1055.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1056,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1056.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1057,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1057.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1058,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1058.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1059,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1059.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1060,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1060.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "60_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2321,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2321.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2322,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2322.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2323,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2323.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2324,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2324.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2325,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2325.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2326,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2326.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2327,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2327.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2328,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2328.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2329,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2329.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2330,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2330.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2331,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2331.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2332,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2332.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2333,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2333.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2334,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2334.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2335,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2335.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2336,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2336.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2337,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2337.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2338,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2338.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2339,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2339.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2340,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2340.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "62_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 661,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/661.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 662,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/662.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 663,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/663.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 664,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/664.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 665,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/665.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 666,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/666.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 667,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/667.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 668,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/668.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 669,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/669.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 670,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/670.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 671,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/671.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 672,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/672.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 673,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/673.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 674,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/674.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 675,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/675.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 676,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/676.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 677,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/677.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 678,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/678.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 679,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/679.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 680,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/680.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "63_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 981,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/981.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 982,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/982.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 983,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/983.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 984,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/984.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 985,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/985.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 986,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/986.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 987,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/987.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 988,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/988.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 989,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/989.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 990,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/990.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 991,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/991.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 992,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/992.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 993,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/993.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 994,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/994.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 995,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/995.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 996,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/996.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 997,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/997.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 998,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/998.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 999,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/999.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1000,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1000.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "64_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 641,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/641.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 642,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/642.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 643,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/643.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 644,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/644.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 645,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/645.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 646,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/646.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 647,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/647.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 648,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/648.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 649,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/649.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 650,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/650.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 651,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/651.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 652,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/652.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 653,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/653.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 654,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/654.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 655,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/655.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 656,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/656.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 657,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/657.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 658,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/658.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 659,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/659.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 660,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/660.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "66_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 801,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/801.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 802,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/802.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 803,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/803.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 804,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/804.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 805,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/805.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 806,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/806.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 807,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/807.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 808,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/808.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 809,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/809.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 810,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/810.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 811,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/811.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 812,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/812.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 813,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/813.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 814,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/814.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 815,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/815.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 816,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/816.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 817,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/817.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 818,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/818.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 819,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/819.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 820,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/820.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "71_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2141,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2141.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2142,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2142.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2143,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2143.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2144,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2144.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2145,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2145.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2146,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2146.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2147,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2147.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2148,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2148.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2149,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2149.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2150,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2150.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2151,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2151.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2152,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2152.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2153,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2153.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2154,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2154.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2155,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2155.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2156,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2156.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2157,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2157.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2158,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2158.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2159,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2159.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2160,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2160.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "75_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2381,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2381.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2382,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2382.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2383,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2383.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2384,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2384.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2385,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2385.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2386,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2386.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2387,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2387.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2388,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2388.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2389,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2389.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2390,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2390.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2391,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2391.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2392,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2392.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2393,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2393.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2394,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2394.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2395,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2395.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2396,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2396.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2397,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2397.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2398,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2398.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2399,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2399.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2400,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2400.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "86_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 1101,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1101.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 1102,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1102.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 1103,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1103.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 1104,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1104.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 1105,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1105.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 1106,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1106.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 1107,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1107.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 1108,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1108.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 1109,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1109.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 1110,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1110.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 1111,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1111.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 1112,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1112.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 1113,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1113.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 1114,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1114.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 1115,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1115.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 1116,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1116.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 1117,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1117.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 1118,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1118.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 1119,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1119.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 1120,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/1120.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "93_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 2341,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2341.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 2342,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2342.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 2343,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2343.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 2344,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2344.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 2345,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2345.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 2346,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2346.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 2347,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2347.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 2348,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2348.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 2349,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2349.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 2350,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2350.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 2351,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2351.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 2352,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2352.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 2353,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2353.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 2354,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2354.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 2355,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2355.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 2356,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2356.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 2357,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2357.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 2358,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2358.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 2359,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2359.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 2360,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/2360.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  },
  {
      "filename": "97_instances_train2018.json",
      "imagesSize": 20,
      "categorySet": [
          21
      ],
      "images": [
          {
              "id": 741,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/741.jpg",
              "doneness": [
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0
              ]
          },
          {
              "id": 742,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/742.jpg",
              "doneness": [
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1,
                  1
              ]
          },
          {
              "id": 743,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/743.jpg",
              "doneness": [
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2,
                  2
              ]
          },
          {
              "id": 744,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/744.jpg",
              "doneness": [
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3,
                  3
              ]
          },
          {
              "id": 745,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/745.jpg",
              "doneness": [
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4,
                  4
              ]
          },
          {
              "id": 746,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/746.jpg",
              "doneness": [
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5,
                  5
              ]
          },
          {
              "id": 747,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/747.jpg",
              "doneness": [
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6,
                  6
              ]
          },
          {
              "id": 748,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/748.jpg",
              "doneness": [
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7,
                  7
              ]
          },
          {
              "id": 749,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/749.jpg",
              "doneness": [
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8,
                  8
              ]
          },
          {
              "id": 750,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/750.jpg",
              "doneness": [
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9,
                  9
              ]
          },
          {
              "id": 751,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/751.jpg",
              "doneness": [
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10,
                  10
              ]
          },
          {
              "id": 752,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/752.jpg",
              "doneness": [
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11,
                  11
              ]
          },
          {
              "id": 753,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/753.jpg",
              "doneness": [
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12,
                  12
              ]
          },
          {
              "id": 754,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/754.jpg",
              "doneness": [
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13,
                  13
              ]
          },
          {
              "id": 755,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/755.jpg",
              "doneness": [
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14,
                  14
              ]
          },
          {
              "id": 756,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/756.jpg",
              "doneness": [
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15,
                  15
              ]
          },
          {
              "id": 757,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/757.jpg",
              "doneness": [
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16,
                  16
              ]
          },
          {
              "id": 758,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/758.jpg",
              "doneness": [
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17,
                  17
              ]
          },
          {
              "id": 759,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/759.jpg",
              "doneness": [
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18,
                  18
              ]
          },
          {
              "id": 760,
              "image": "http://47.106.93.196/rawfood_coco_all/train2018/760.jpg",
              "doneness": [
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19,
                  19
              ]
          }
      ]
  }
];

// 旧的食材
// export const allCategory = [
//   {
//     "id": 1,
//     "name": "TuSi"
//   },
//   {
//     "id": 2,
//     "name": "XiangChang"
//   },
//   {
//     "id": 3,
//     "name": "ZhuPaiGu"
//   },
//   {
//     "id": 4,
//     "name": "ZhuRou"
//   },
//   {
//     "id": 5,
//     "name": "YouYu"
//   },
//   {
//     "id": 6,
//     "name": "ZhengYu"
//   },
//   {
//     "id": 7,
//     "name": "YuMi"
//   },
//   {
//     "id": 8,
//     "name": "HongShuZiShu"
//   },
//   {
//     "id": 9,
//     "name": "HuaSheng"
//   },
//   {
//     "id": 10,
//     "name": "JiChiGen"
//   },
//   {
//     "id": 11,
//     "name": "JiQuanChi"
//   },
//   {
//     "id": 12,
//     "name": "JiZhongChi"
//   },
//   {
//     "id": 13,
//     "name": "JiXiongRou"
//   },
//   {
//     "id": 14,
//     "name": "JiTui"
//   },
//   {
//     "id": 15,
//     "name": "ZhengJi"
//   },
//   {
//     "id": 16,
//     "name": "PeiGenJuan"
//   },
//   {
//     "id": 17,
//     "name": "TuDou"
//   },
//   {
//     "id": 18,
//     "name": "MaoMaoChongMianBao"
//   },
//   {
//     "id": 19,
//     "name": "NiuJiaoMianBao"
//   },
//   {
//     "id": 20,
//     "name": "ZhiBeiDanGao"
//   },
//   {
//     "id": 21,
//     "name": "DanTa"
//   },
//   {
//     "id": 22,
//     "name": "QuQiBing"
//   },
//   {
//     "id": 23,
//     "name": "BingGan"
//   },
//   {
//     "id": 24,
//     "name": "Pizza"
//   },
//   {
//     "id": 25,
//     "name": "NiuPa"
//   },
//   {
//     "id": 26,
//     "name": "YangPai"
//   },
//   {
//     "id": 27,
//     "name": "SanWenYuPa"
//   },
//   {
//     "id": 28,
//     "name": "Xia"
//   },
//   {
//     "id": 29,
//     "name": "QiFengDanGao"
//   },
//   {
//     "id": 30,
//     "name": "GuangShiYueBing"
//   },
//   {
//     "id": 31,
//     "name": "ZhuTi"
//   },
//   {
//     "id": 32,
//     "name": "Ya"
//   },
//   {
//     "id": 33,
//     "name": "QieZi"
//   },
//   {
//     "id": 34,
//     "name": "KeSongMianBao"
//   },
//   {
//     "id": 100,
//     "name": "KongZai"
//   }
// ];


export const allCategory = [
  {
    "id": 1,
    "name": "TuSi"
  },
  {
    "id": 2,
    "name": "TaiShiXiangChang"
  },
  {
    "id": 47,
    "name": "MeiShiXiangChang"
  },
  {
    "id": 48,
    "name": "HuaDaoTaiShiXiangChang"
  },
  {
    "id": 49,
    "name": "HuaDaoMeiShiXiangChang"
  },
  {
    "id": 50,
    "name": "HuaDaoXiangLaTaiShiXiangChang"
  },
  {
    "id": 51,
    "name": "XiangLaTaiShiXiangChang"
  },
  {
    "id": 79,
    "name": "GuangShiLaChang"
  },
  {
    "id": 80,
    "name": "QiePianHongChang"
  },
  {
    "id": 81,
    "name": "HuaDaoZhongShiHuoTuiChang"
  },
  {
    "id": 82,
    "name": "ZhongShiHuoTuiChang"
  },
  {
    "id": 83,
    "name": "QieDuanSiChuanLaChang"
  },
  {
    "id": 84,
    "name": "BaiChang"
  },
  {
    "id": 85,
    "name": "HuaDaoSiChuanLaChang"
  },
  {
    "id": 86,
    "name": "QiePianBaiChang"
  },
  {
    "id": 87,
    "name": "HuaDaoGuangShiLaChang"
  },
  {
    "id": 3,
    "name": "MiZhiPaiGu,XinAoErLiangPaiGu"
  },
  {
    "id": 37,
    "name": "SuanXiangPaiGu"
  },
  {
    "id": 38,
    "name": "XiangLaPaiGu,ZiRanPaiGu"
  },
  {
    "id": 52,
    "name": "JiangXiangKaoPaiGu,HeiJiaoPaiGu"
  },
  {
    "id": 4,
    "name": "MiZhiChaShao"
  },
  {
    "id": 53,
    "name": "HeiJiaoDaiGu"
  },
  {
    "id": 54,
    "name": "HeiJiaoBuDaiGu"
  },
  {
    "id": 5,
    "name": "JiangKaoYouYu"
  },
  {
    "id": 56,
    "name": "JiangKaoYouYuXu"
  },
  {
    "id": 6,
    "name": "ZhengYu"
  },
  {
    "id": 7,
    "name": "YuMi"
  },
  {
    "id": 8,
    "name": "hongShuZhengTiao"
  },
  {
    "id": 57,
    "name": "ZiShuZhengTiao"
  },
  {
    "id": 58,
    "name": "HongShuQieDuan"
  },
  {
    "id": 59,
    "name": "ZiShuQieDuan"
  },
  {
    "id": 60,
    "name": "HongShuDuiBanQie"
  },
  {
    "id": 61,
    "name": "ZiShuDuiBanQie"
  },
  {
    "id": 9,
    "name": "FenPiXiangCuiHuaShengMi"
  },
  {
    "id": 62,
    "name": "HongPiXiangCuiHuaShengMi"
  },
  {
    "id": 35,
    "name": "FenPiWuXiangHuaShengMi"
  },
  {
    "id": 63,
    "name": "HongPiWuXiangHuaShengMi"
  },
  {
    "id": 64,
    "name": "DaiQiaoFenPiXiangCuiHuaShengMi"
  },
  {
    "id": 65,
    "name": "DaiQiaoHongPiXiangCuiHuaShengMi"
  },
  {
    "id": 66,
    "name": "DaiQiaoFenPiWuXiangHuaShengMi"
  },
  {
    "id": 67,
    "name": "DaiQiaoHongPiWuXiangHuaShengMi"
  },
  {
    "id": 88,
    "name": "HeiPiXiangCuiHuaShengMi"
  },
  {
    "id": 10,
    "name": "XinAoErLiangJiChiGen"
  },
  {
    "id": 68,
    "name": "XiangLaJiChiGen"
  },
  {
    "id": 69,
    "name": "HeiJiaoJiChiGen"
  },
  {
    "id": 11,
    "name": "XinAoErLiangJiQuanChi"
  },
  {
    "id": 70,
    "name": "XiangLaJiQuanChi"
  },
  {
    "id": 71,
    "name": "MiZhiJiQuanChi"
  },
  {
    "id": 72,
    "name": "HeiJiaoJiZhongChi,WuXiangJiZhongChi"
  },
  {
    "id": 12,
    "name": "MiZhiJiChi"
  },
  {
    "id": 39,
    "name": "XinAoErLiangJiChi"
  },
  {
    "id": 40,
    "name": "XiangLaJiChi"
  },
  {
    "id": 73,
    "name": "HeiJiaoJiZhongChi,ZiRanJiZhongChi,WuXiangJiZhongChi"
  },
  {
    "id": 89,
    "name": "SuanXiangJiZhongChi"
  },
  {
    "id": 90,
    "name": "RiShiJiZhongChi"
  },
  {
    "id": 91,
    "name": "GaLiJiZhongChi"
  },
  {
    "id": 92,
    "name": "YanJuJiZhongChi"
  },
  {
    "id": 13,
    "name": "JiXiongRou"
  },
  {
    "id": 14,
    "name": "XinAoErLiangJiTui"
  },
  {
    "id": 74,
    "name": "XiangLaJiTui"
  },
  {
    "id": 75,
    "name": "MiZhiKaoJiTui"
  },
  {
    "id": 15,
    "name": "ZhengJi"
  },
  {
    "id": 16,
    "name": "JinZhenGuPeiGenJuan"
  },
  {
    "id": 46,
    "name": "PeiGenShuCaiJuan"
  },
  {
    "id": 17,
    "name": "TuDouZhengGe"
  },
  {
    "id": 76,
    "name": "TuDouQieKuai"
  },
  {
    "id": 77,
    "name": "TuDouQieTiao"
  },
  {
    "id": 78,
    "name": "TuDouQiePian"
  },
  {
    "id": 18,
    "name": "MaoMaoChongMianBao"
  },
  {
    "id": 45,
    "name": "YeRongMianBao"
  },
  {
    "id": 93,
    "name": "ChangTiaoMianBao"
  },
  {
    "id": 94,
    "name": "XiaoCanBao"
  },
  {
    "id": 19,
    "name": "NiuJiaoMianBao"
  },
  {
    "id": 20,
    "name": "ZhiBeiDanGao"
  },
  {
    "id": 21,
    "name": "DaHaoPuShiDanTa"
  },
  {
    "id": 106,
    "name": "XiaoHaoPuShiDanTa"
  },
  {
    "id": 22,
    "name": "YuanWeiQuQi"
  },
  {
    "id": 41,
    "name": "MoChaQuQi"
  },
  {
    "id": 95,
    "name": "QiaoKeLiQuQi"
  },
  {
    "id": 23,
    "name": "ManYueMeiBingGan"
  },
  {
    "id": 24,
    "name": "PiSa"
  },
  {
    "id": 25,
    "name": "NiuBa"
  },
  {
    "id": 26,
    "name": "YangPai"
  },
  {
    "id": 27,
    "name": "YuanWeiSanWenYuYuanQiePai"
  },
  {
    "id": 36,
    "name": "YuanWeiSanWenYuLiu"
  },
  {
    "id": 96,
    "name": "DeSeBeiGeDeSanWenYuYuanQiePai"
  },
  {
    "id": 97,
    "name": "DeSeBeiGeDeSanWenYuLiu"
  },
  {
    "id": 98,
    "name": "DaiPeiCaiDeYuanWeiSanWenYuYuanQiePai"
  },
  {
    "id": 99,
    "name": "DaiPeiCaiDeYuanWeiSanWenYuLiu"
  },
  {
    "id": 105,
    "name": "MaLaShangTangSanWenYuYuanQiePai"
  },
  {
    "id": 28,
    "name": "SuanRongXia"
  },
  {
    "id": 42,
    "name": "SuanRongKaoXiaChuan"
  },
  {
    "id": 43,
    "name": "ZhiShiJuXia"
  },
  {
    "id": 44,
    "name": "ZhiShiJuAGenTingHongXia"
  },
  {
    "id": 100,
    "name": "YanKaoXia"
  },
  {
    "id": 101,
    "name": "XiangLaKaoXiaChuan"
  },
  {
    "id": 102,
    "name": "YanKaoXiaChuan"
  },
  {
    "id": 103,
    "name": "XiangLaKaoXia"
  },
  {
    "id": 104,
    "name": "YanKaoAGenTingHongXia"
  },
  {
    "id": 29,
    "name": "QiFengDanGao"
  },
  {
    "id": 30,
    "name": "GuangShiYueBing"
  },
  {
    "id": 31,
    "name": "ZhuTi"
  },
  {
    "id": 32,
    "name": "Ya"
  },
  {
    "id": 33,
    "name": "QieZi"
  },
  {
    "id": 34,
    "name": "KeSongMianBao"
  },
  {
    "id": 100,
    "name": "KongZai"
  }
];
