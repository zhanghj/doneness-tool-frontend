import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

class IndexPage extends Component {
  // static propTypes = {
  //   prop: PropTypes
  // }

  render() {
    const { dispatch } = this.props;

    return (
      // <div className={styles.normal}>
      //   <h1 className={styles.title}>Yay! Welcome to dva!</h1>
      //   <div className={styles.welcome} />
      //   <ul className={styles.list}>
      //     <li>To get started, edit <code>src/index.js</code> and save to reload.</li>
      //     <li><a href="https://github.com/dvajs/dva-docs/blob/master/v1/en-us/getting-started.md">Getting Started</a></li>
      //   </ul>
      // </div>
      <ul style={{margin: '20px'}}>
        <li><a href='javascript:;' onClick={() => dispatch(routerRedux.push('/show'))}>演示</a></li>
        <li><a href='javascript:;' onClick={() => dispatch(routerRedux.push('/summary'))}>SummaryPage</a></li>
        <li><a href='javascript:;' onClick={() => dispatch(routerRedux.push('/select'))}>TestPage</a></li>
        <li><a href='javascript:;' onClick={() => dispatch(routerRedux.push('/annotation/list'))}>AnnotationPage</a></li>
        <li><a href='javascript:;' onClick={() => dispatch(routerRedux.push('/evaluation/list'))}>EvaluationPage</a></li>
      </ul>
    );
  }
}

export default connect()(IndexPage);
