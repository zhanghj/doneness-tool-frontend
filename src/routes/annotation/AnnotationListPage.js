import React, { Component } from 'react';
import { Select, Table, Input } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import LazyLoad from 'react-lazyload';
import { getCategoryNames } from '../../utils/categoryUtil';

const Option = Select.Option;

class AnnotationListPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  constructor() {
    super();
    this.state = {
      categoryId: '',
      filter: '',
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    // const { category } = this.props.annotation;
    // if(!category){
    //   dispatch({
    //     type: 'annotation/fetchAllCategory',
    //   });
    // }
    dispatch({
      type: 'annotation/fetchAllAnnotationFiles',
    });
  }

  handleCategoryChange = (value) => {
    const { dispatch } = this.props;
    // console.log(value);
    dispatch({
      type: 'annotation/handleCategoryChange',
      payload: {
        selectedCategory: value,
      },
    });

  }

  handleAnnotationClick = (value) => {
    const { dispatch } = this.props;

    dispatch(routerRedux.push(`/annotation/detail/${value}`));
  }

  render() {
    const { category, allAnnotationFiles } = this.props.annotation;
    const columns = [
      {
        title: 'filename',
        dataIndex: 'filename',
      },
      {
        title: 'imagesSize',
        dataIndex: 'imagesSize',
      },
      {
        title: 'categorySet',
        dataIndex: 'categorySet',
        render: categorySet => getCategoryNames(categorySet),
      },
    ];

    const expandedRowRender = (record) => {

      const columns = [
        { title: 'id', dataIndex: 'id', key: 'id' },
        { title: 'image', dataIndex: 'image',
          render: image => (
            <LazyLoad height={200} once offset={100}>
              <img height={200} alt={''} src={image} />
            </LazyLoad>
          ),
        },
        { title: 'doneness', dataIndex: 'doneness', render: doneness => doneness.join(',') },
      ];

      const data = record.images;

      return (
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      );
    };

    // allAnnotationFiles 过滤
    let dataSource = allAnnotationFiles;
    const { categoryId, filter } = this.state;
    if(categoryId) {
      dataSource = dataSource.filter((annotation) => {
        return annotation.categorySet.some((category) => {
          return category == categoryId;
        })
      })
    }
    if(filter) {
      dataSource = dataSource.filter((annotation) => {
        return annotation.filename.indexOf(filter) > -1;
      })
    }

    return (
      <div style={{ margin: 20}}>
        category：<Select style={{ width: 200 }} onChange={(value) => this.setState({categoryId:value,})} value={this.state.categoryId} allowClear={true}>
          { category && category.map(item => <Option value={item.id}>{item.id},{item.name}</Option>)}
        </Select>

        filter by name: <Input style={{ width: 200}} placeholder="filter by name" onChange={(event) => this.setState({filter:event.target.value,})} value={this.state.filter}/>

        <Table
          dataSource={dataSource}
          columns={columns}
          pagination={false}
          rowKey={record => record.method}
          expandedRowRender={expandedRowRender}
        />
      </div>
    )
  }
}

export default connect(({annotation}) => {
  return {annotation};
})(AnnotationListPage);
