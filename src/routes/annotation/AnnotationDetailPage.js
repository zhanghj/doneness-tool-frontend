import React, { Component } from 'react';
import LazyLoad from 'react-lazyload';
import { Table } from 'antd';
import { getAnnotationDetail } from '../../services/annotation';

export default class AnnotationDetailPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  state = {
    dataSource: [],
  }

  componentDidMount() {
    const { match } = this.props;
    const { params } = match;
    const { filename } = params;
    console.log(params);

    getAnnotationDetail(filename).then(dataSource => this.setState({dataSource}));
  }

  render() {
    const { dataSource } = this.state;
    const columns = [{
      title: '图片',
      dataIndex: 'image',
      key: 'image',
      render: image => (
        <LazyLoad height={200} once offset={100}>
          <img height={200} src={image} alt={''}/>
        </LazyLoad>
      )
    }, {
      title: 'doneness',
      dataIndex: 'doneness',
      key: 'doneness',
      render: doneness => {
        return doneness.join(',');
      }
    }];

    return (
      <div>
        <Table dataSource={dataSource} columns={columns} pagination={false} rowKey={record => record.image}></Table>
      </div>
    )
  }
}
