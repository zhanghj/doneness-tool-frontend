import React, { Component, Fragment } from 'react';

import styles from './ShowPage.less';
import PollImage from '../components/PollImage';
import MaskPollImage from '../components/MaskPollImage';
import { getShowSN } from '../services/analysis';

export default class ShowPage extends Component {
  static propTypes = {

  }

  state = {
    showSN: [],
  }

  componentDidMount() {
    getShowSN().then(showSN => this.setState({showSN}))
  }

  render() {
    const images = this.state.showSN.map(sn => {
      return <Fragment>
        <div className={styles['show-item']}>
          <PollImage sn={sn} />
        </div>
        <div className={styles['show-item']}>
          <MaskPollImage sn={sn} />
        </div>
      </Fragment>
    })

    return (
      <div className={styles['show-wrap']}>
        {images}
      </div>
    )
  }
}
