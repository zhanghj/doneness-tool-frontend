import React, { Component } from 'react';
import { Table } from 'antd';
import { connect } from 'dva';

class ConfusionStatsPage extends Component {

  render() {
    const { confusionStats } = this.props;

    const head = `ConfusionMatrixt statistics`;

    const columns = [
      { title: 'categoryName', dataIndex: 'categoryName', key: 'categoryName' },
      { title: 'matchCount', dataIndex: 'matchCount', key: 'matchCount' },
      { title: 'totalCount', dataIndex: 'totalCount', key: 'totalCount' },
      { title: 'matchPercent', dataIndex: 'matchPercent', key: 'matchPercent' },
      { title: 'rawData', dataIndex: 'rawData', key: 'rawData' },
    ];
    return (
      <React.Fragment>
        <h3>{head}</h3>
        <Table
          columns={columns}
          dataSource={confusionStats.stats}
          pagination={false}
        />
      </React.Fragment>
    )
  }
}

export default connect(({confusionStats}) => {
  return {confusionStats};
})(ConfusionStatsPage);
