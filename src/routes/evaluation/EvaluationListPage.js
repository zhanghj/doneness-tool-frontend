import React, { Component } from 'react';
import { Select, Table, Cascader } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import LazyLoad from 'react-lazyload';
import DatasetStatsPage from './DatasetStatsPage';
import styles from './EvaluationListPage.less';
import ConfusionStatsPage from './ConfusionStatsPage';

const Option = Select.Option;

class EvaluationListPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'evaluation/fetchEvaluationDirs',
    });
    dispatch({
      type: 'evaluation/fetchCategories',
    });
  }

  handleDirChange = (value) => {
    const { dispatch } = this.props;
    if(value) {
      dispatch({
        type: 'evaluation/save',
        payload: {
          key: 'selectedDir',
          value,
        },
      });
      dispatch({
        type: 'evaluation/fetchEvaluationResult',
      });
      dispatch({
        type: 'confusionStats/fetchConfustionStats',
        payload: {
          dir: value,
        }
      });
    }
  }

  handleCategoryChange = (value) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'evaluation/save',
      payload: {
        key: 'selectedCategoryId',
        value,
      },
    });
    dispatch({
      type: 'evaluation/fetchEvaluationResult',
    });
  }

  handleAnnotationClick = (value) => {
    const { dispatch } = this.props;
    const { selectedCategory } = this.props.annotation;

    dispatch(routerRedux.push(`/annotation/${selectedCategory}/${value}`));
  }

  render() {
    const { dirs, category, selectedCategoryId, evaluationResult } = this.props.evaluation;

    const data = evaluationResult;

    const columns = [
      { title: '文件名', dataIndex: 'filename', key: 'filename' },
      { title: 'imagesSize', dataIndex: 'imagesSize', key: 'imagesSize' },
    ];

    const expandedRowRender = (record) => {
      const { evaluationResult } = this.props.evaluation;
      const result = evaluationResult.filter(result => {
        return result.filename === record.filename;
      });

      const columns = [
        { title: 'imageId', dataIndex: 'imageId', key: 'imageId' },
        { title: 'image', dataIndex: 'imageUrl', key: 'imageUrl' ,
          render: image => (
            <LazyLoad height={200} once offset={100}>
              <img height={200} alt={''} src={image} />
            </LazyLoad>
          ),
        },
        { title: 'annotationsSize', dataIndex: 'annotationsSize', key: 'annotationsSize' },
      ];

      const data = result[0].detailList;

      return (
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      );
    };

    return (
      <div className={styles['eval-wrapper']} >
        <div className={styles['eval-left']}>
          <DatasetStatsPage type={'train'} />
          <DatasetStatsPage type={'val'} />
          <DatasetStatsPage type={'test'} />
        </div>
        <div className={styles['eval-right']} style={{ margin: 20}}>
          {/* 文件夹：<Select style={{ width: 200 }} onChange={this.handleDirChange} value={selectedDir}>
            { dirs && dirs.map(c => <Option value={c}>{c}</Option>)}
          </Select> */}
          文件夹：<Cascader options={dirs} onChange={this.handleDirChange} allowClear={false}/>
          category：<Select style={{ width: 200 }} onChange={this.handleCategoryChange} value={selectedCategoryId}>
            { category && category.map(c => <Option value={c.id}>{c.name}</Option>)}
          </Select>

          <h3>Evaluation Data</h3>
          <Table
            columns={columns}
            expandedRowRender={expandedRowRender}
            dataSource={data}
            pagination={false}
          />

          <ConfusionStatsPage />
          {/* <Table dataSource={annotationFilenames} columns={columns} pagination={false}></Table> */}
        </div>
      </div>
    )
  }
}

export default connect(({evaluation}) => {
  return {evaluation};
})(EvaluationListPage);
