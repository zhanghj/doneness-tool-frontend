import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import { connect } from 'dva';

class DatasetStatsPage extends Component {
  static propTypes = {
    type: PropTypes.oneOf(['train', 'val', 'test']).isRequired,
  }

  componentDidMount() {
    const { dispatch, type } = this.props;
    dispatch({
      type: 'datasetStats/fetchDatasetStats',
      payload: {
        type,
      },
    });
  }

  render() {
    const { type, datasetStats } = this.props;

    // const data = type === 'train' ? datasetStats.train : datasetStats.val ;
    const data = datasetStats[type] ;
    const head = `${type} dataset statistics`;

    const columns = [
      { title: 'categoryName', dataIndex: 'categoryName', key: 'categoryName', render: (text, record, index) => (`${record.categoryId},${record.categoryName}`)},
      { title: 'annotationsSize', dataIndex: 'annotationsSize', key: 'annotationsSize', render: (text, record, index) => (Math.round(record.imagesSize / 20)), },
      { title: 'imagesSize', dataIndex: 'imagesSize', key: 'imagesSize' },
      { title: 'instancesSize', dataIndex: 'instancesSize', key: 'instancesSize' },
    ];

    const footer = (currentPageData) => {
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      const imagesSize = currentPageData.map(data => data.imagesSize).reduce(reducer, 0);
      const instancesSize = currentPageData.map(data => data.instancesSize).reduce(reducer, 0);
      return `total: annotationSize ${Math.round(imagesSize/20)}, imagesSize ${imagesSize}, instancesSize ${instancesSize}`;
    };

    return (
      <div style={{ margin: 20}}>
        <h3>{head}</h3>
        {/* <Table dataSource={annotationFilenames} columns={columns} pagination={false}></Table> */}
        <Table
          columns={columns}
          dataSource={data}
          pagination={false}
          footer={footer}
        />
      </div>
    )
  }
}

export default connect(({datasetStats}) => {
  return {datasetStats};
})(DatasetStatsPage);
