import React, { Component, Fragment } from 'react';

import { connect } from 'dva';
import { Table } from 'antd';
import { getCategoryName } from '../../utils/categoryUtil';

class DeltaPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'delta/fetchDeltaData',
    });
  }

  render() {
    const { delta } = this.props
    const tables = delta.map(item => {
      const columns = [
        {
          title: 'category',
          dataIndex: 'category',
          render: text => getCategoryName(text),
        },
        {
          title: 'total',
          dataIndex: 'total',
        }
      ];

      const expandedRowRender = (record) => {

        const columns = [
          { title: 'cooktime', dataIndex: 'cooktime', },
          { title: 'time1', dataIndex: 'time1', },
          { title: 'time1Delta', dataIndex: 'time1Delta', },
          { title: 'time2', dataIndex: 'time2', },
          { title: 'time2Delta', dataIndex: 'time2Delta', },
          { title: 'time3', dataIndex: 'time3', },
          { title: 'time3Delta', dataIndex: 'time3Delta', },
          { title: 'time4', dataIndex: 'time4', },
          { title: 'time4Delta', dataIndex: 'time4Delta', },
        ];

        const data = record.list;

        return (
          <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            scroll={{x:true,}}
          />
        );
      };

      return (
        <Fragment>
          <h3>{item.method}</h3>
          <Table
            dataSource={item.list}
            columns={columns}
            pagination={false}
            expandedRowRender={expandedRowRender}
          ></Table>
        </Fragment>)
    });

    return (
      <Fragment>
        <h2>Delta</h2>

        { tables }
      </Fragment>
    )
  }
}

export default connect(({delta}) => {
  return { delta: delta.data };
})(DeltaPage);
