import React, { Component, Fragment } from 'react';

import { Table } from 'antd';
import { getSummaryData } from '../../services/analysis';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import styles from './SummaryPage.less';
import DeltaPage from './DeltaPage';

class SummaryPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  state = {
    // data: [
    //   {
    //     "DanTa": "0.6308110924564349",
    //     "BingGan": "0.8308675184936113",
    //     "Xia": "0.5942619398457137",
    //     "method": "1step",
    //   },ç
    //   {
    //     "DanTa": "0.6308110924564349",
    //     "BingGan": "0.8308675184936113",
    //     "Xia": "0.5942619398457137",
    //     "method": "2step_doneness_deltanet_densenet121_regression_concat_loss0.37_grayperc0.01",
    //   },
    // ],
  };

  componentDidMount() {
    getSummaryData().then(dataSource => {
      this.setState({
        dataSource,
      });
    });
  }

  handleMethodClick = (method) => {
    const { dispatch } = this.props;
    dispatch(routerRedux.push(`/select/${method}`))
  }

  getColumns = (dataSource) => {
    let columns = [];

    columns.push({
      title: 'method',
      dataIndex: 'method',
      key: 'method',
      fixed: 'left',
      // width: 200,
      render: text => <a href="javascript:;" onClick={() => this.handleMethodClick(text)}>{text}</a>,
    });

    const keys = Object.keys(dataSource[0]).filter(key => key !== 'method');
    keys.forEach(key => {
      columns.push({
        title: `${key}`,
        dataIndex: key,
        key,
      });
    });

    return columns;
  }

  render() {
    const { dataSource } = this.state;

    if(!dataSource){
      return <div></div>;
    }
    // const keys = Object.keys(dataSource[0]).filter(key => key !== 'method');

    // return (
    //   <div>
    //     <table>
    //       <thead>
    //         <tr>
    //           <th className={styles['td-style']}>method</th>
    //           {
    //             keys.map(key => {
    //               return (
    //                 <th className={styles['td-style']}>{`${key} ACCURACY`}</th>
    //               )
    //             })
    //           }
    //         </tr>
    //       </thead>
    //       <tbody>
    //         {
    //           dataSource.map(item => {
    //             return (
    //               <tr className={styles['text-center']}>
    //                 <td className={styles['td-style']}>{item['method']}</td>
    //                 {
    //                   keys.map(key =>
    //                     <td className={styles['td-style']}>{item[key]}</td>
    //                   )
    //                 }
    //               </tr>
    //             );
    //           })
    //         }
    //       </tbody>
    //     </table>
    //   </div>
    // )

    const columns = this.getColumns(dataSource);

    return (
      <Fragment>
        <h2>ACCURACY</h2>
        <Table dataSource={dataSource} columns={columns} pagination={false} rowKey={record => record.method} scroll={{x:true,}}></Table>

        <DeltaPage/>
      </Fragment>
    )
  }
}

export default connect()(SummaryPage);
