import React, { Component } from 'react';

// import data from './ResultData';
import { Table } from 'antd';
import { connect } from 'dva';
import { getCategoryName, checkNumber } from '../../utils/categoryUtil';
import { isNumber } from 'util';

class PlaybackTimeIntervalPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  componentDidMount() {
    const { model, time, dispatch } = this.props;

    dispatch({
      type: 'playback/fetchTimeInterval',
      payload: {
        model,
        time,
      }
    });
  }

  render() {
    const data = this.props.playback.timeInterval;
    const columns = [
      {
        title: '实验结果',
        dataIndex: 'experiment',
      },
      {
        title: '人为记录结果',
        dataIndex: 'human',
        render: (human) => {
          if(checkNumber(human)){
            return getCategoryName(human)
          }
          return human
        },
      }
    ];

    return (
      <div>
        <h2>TimeInterval</h2>
        <Table dataSource={data} columns={columns} pagination={false}></Table>
      </div>
    )
  }
}

export default connect(({playback}) => {
  return {playback};
})(PlaybackTimeIntervalPage);
