import React, { Component } from 'react';
import LazyLoad from 'react-lazyload';

// import data from './ResultData';
import { Table, Form, Switch } from 'antd';
import { connect } from 'dva';
import { getCategoryName } from '../../utils/categoryUtil';
import PlaybackTimeIntervalPage from './PlaybackTimeIntervalPage';

const FormItem = Form.Item;

class PlaybackDetailPage extends Component {
  static propTypes = {
    // prop: PropTypes
  }

  state = {
    showDiffDoneness: true,
    showDiffCategory: true,
  }

  componentDidMount() {
    const { match, dispatch } = this.props;
    const { params } = match;
    const { model, time } = params;

    dispatch({
      type: 'playback/fetchCookData',
      payload: {
        model,
        time,
      }
    });
  }

  // state = {
  //   title: '2step_doneness_deltanet_densenet121_regression_concat_loss0.37_grayperc0.01',
  // }

  render() {

    const { model, time } = this.props.match.params;
    const { showDiffDoneness, showDiffCategory } = this.state;
    let { detailData: data, firstFramePrefix } = this.props.playback;
    // const model = '2step_doneness_deltanet_densenet121_regression_concat_loss0.37_grayperc0.01',
    //       cook = '2018_08_11_16_31_29';

    const getFileUrl = (model, time, filename) => {
      return `http://47.106.93.196/gap_analysis/result/${model}/${time}/${filename}`;
    }

    const getResFig = (model, time) => {
      const url = `http://47.106.93.196/gap_analysis/result/${model}/${time}/res_fig.png`;
      return <img src={url} alt={'分割图，看不到就是图片不存在'} height={200} />
    }

    const columns = [
      {
        title: '图片',
        dataIndex: 'filename_img',
        key: 'filename_img',
        render: filename => (
          <LazyLoad height={200} once offset={100}>
            <img src={getFileUrl(model, time, filename)} alt={''} />
          </LazyLoad>
        )
      },
      {
        title: '参考图片',
        render: (_, record) => {
          const index = record.filename.lastIndexOf('_');
          const firstFilename = firstFramePrefix + record.filename.substring(index);
          return (
            <LazyLoad height={200} once offset={100}>
              <img src={getFileUrl(model, time, firstFilename)} alt={''} />
            </LazyLoad>);
        }
      },
      {
        title: '图片文件名',
        dataIndex: 'filename',
        key: 'filename',
      },
      {
        title: '机器识别类别',
        dataIndex: 'machine_category',
        key: 'machine_category',
        render: (machine_category) => getCategoryName(machine_category),
      },
      {
        title: '人眼识别类别',
        dataIndex: 'human_category',
        key: 'human_category',
        render: (human_category) => getCategoryName(human_category),
      },
      {
        title: '机器识别结果',
        dataIndex: 'machine_doneness',
        key: 'machine_doneness',
      },
      {
        title: '人眼识别结果',
        dataIndex: 'human_doneness',
        key: 'human_doneness',
      }
    ];

    data = data.map(d => {
      d.filename_img = d.filename;
      return d;
    });

    // 需要过滤显示
    if(showDiffDoneness || showDiffCategory){
      data = data.filter(d => {
        return (showDiffDoneness && d.human_doneness !== d.machine_doneness) ||
               (showDiffCategory && d.human_category !== d.machine_category)
      });
    }

    return (
      <div>
        <h2>{model}</h2>
        <Form layout="inline">
            <FormItem label="显示不相等的熟度识别结果">
              <Switch checked={showDiffDoneness} onChange={() => this.setState({showDiffDoneness: !showDiffDoneness})} />
            </FormItem>
            <FormItem label="显示不相等的种类识别结果">
              <Switch checked={showDiffCategory} onChange={() => this.setState({showDiffCategory: !showDiffCategory})} />
            </FormItem>
        </Form>
        {getResFig(model, time)}
        <PlaybackTimeIntervalPage model={model} time={time}/>
        <Table dataSource={data} columns={columns} pagination={false} rowKey={record => record.filename}></Table>
      </div>
    )
  }
}

export default connect(({playback}) => {
  return {playback};
})(PlaybackDetailPage);
